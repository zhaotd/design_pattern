package com.ysgcs.pattern.iterator.inter;

public interface Iterator {
    public abstract boolean hasNext();
    public abstract Object next();
}
