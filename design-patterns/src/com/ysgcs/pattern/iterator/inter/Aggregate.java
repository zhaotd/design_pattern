package com.ysgcs.pattern.iterator.inter;

public interface Aggregate {
    public abstract Iterator iterator();
}
