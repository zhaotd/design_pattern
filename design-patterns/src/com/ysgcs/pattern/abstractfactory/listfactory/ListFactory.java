package com.ysgcs.pattern.abstractfactory.listfactory;

import com.ysgcs.pattern.abstractfactory.factory.Factory;
import com.ysgcs.pattern.abstractfactory.factory.Link;
import com.ysgcs.pattern.abstractfactory.factory.Page;
import com.ysgcs.pattern.abstractfactory.factory.Tray;

public class ListFactory extends Factory {
    @Override
    public Link createLink(String caption, String url) {
        return new ListLink(caption,url);
    }

    @Override
    public Tray createTray(String caption) {
        return new ListTray(caption);
    }

    @Override
    public Page createPage(String title, String author) {
        return new ListPage(title,author);
    }
}
