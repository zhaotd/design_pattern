import com.ysgcs.pattern.abstractfactory.factory.Factory;
import com.ysgcs.pattern.abstractfactory.factory.Link;
import com.ysgcs.pattern.abstractfactory.factory.Page;
import com.ysgcs.pattern.abstractfactory.factory.Tray;
import com.ysgcs.pattern.iterator.Book;
import com.ysgcs.pattern.iterator.BookShelf;
import com.ysgcs.pattern.iterator.inter.Iterator;

public class Main {

    //迭代器 iterator 测试
    public static void main_a(String[] args) {
        BookShelf bookShelf = new BookShelf(4);
        bookShelf.appendBook(new Book("学习指南"));
        bookShelf.appendBook(new Book("Java编程思想"));
        bookShelf.appendBook(new Book("第一行代码"));
        bookShelf.appendBook(new Book("代码世界"));
        Iterator it = bookShelf.iterator();
        while (it.hasNext()){
            Book book = (Book) it.next();
            System.out.println(book.getName());
        }
    }

    //抽象工厂 abstract factory 测试
    public static void main(String[] args) {
        if(args.length != 1){
            System.out.println("Usage : java Main class.name.of.ConcreteFactory");
            System.out.println("Example : java Main listfactory.ListFactory");
            System.out.println("Example : java Main tablefactory.TableFactory");
        }

        Factory factory = Factory.getFactory("com.ysgcs.pattern.abstractfactory.listfactory.ListFactory");
//        Factory factory = Factory.getFactory(args[0]);

        Link asahi = factory.createLink("朝阳新闻","http://www.asahi.com/");
        Link yomiuri = factory.createLink("读卖新闻","http://www.yomiuri.co.jp/");
        Link us_yahoo = factory.createLink("Yahoo!","http://www.yahoo.com/");
        Link jp_yahoo = factory.createLink("Yahoo!Japan","http://www.yahoo.co.jp/");
        Link excite = factory.createLink("Excite","http://www.excite.com/");
        Link google = factory.createLink("Google","http://www.google.com/");

        Tray traynews = factory.createTray("新闻");
        traynews.add(asahi);
        traynews.add(yomiuri);

        Tray trayyahoo = factory.createTray("Yahoo!");
        trayyahoo.add(us_yahoo);
        trayyahoo.add(jp_yahoo);

        Tray traysearch = factory.createTray("搜索引擎");
        traysearch.add(trayyahoo);
        traysearch.add(excite);
        traysearch.add(google);

        Page page = factory.createPage("LinkPage","艺术攻城狮");
        page.add(traynews);
        page.add(traysearch);
        page.output();
    }
}
